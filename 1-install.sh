#!/bin/bash
clear
echo "    _             _       ___           _        _ _ "
echo "   / \   _ __ ___| |__   |_ _|_ __  ___| |_ __ _| | |"
echo "  / _ \ | '__/ __| '_ \   | || '_ \/ __| __/ _' | | |"
echo " / ___ \| | | (__| | | |  | || | | \__ \ || (_| | | |"
echo "/_/   \_\_|  \___|_| |_| |___|_| |_|___/\__\__,_|_|_|"
echo ""
echo "by Stephan Raabe (2023)"
echo "-----------------------------------------------------"
echo ""
echo "Important: Please make sure that you have followed the "
echo "manual steps in the README to partition the harddisc!"
echo "Warning: Run this script at your own risk."
echo ""

# ------------------------------------------------------
# Enter partition names
# ------------------------------------------------------
lsblk
read -p "Enter the name of the EFI partition (eg. sda1): " sda1
read -p "Enter the name of the ROOT partition (eg. sda2): " sda2
# read -p "Enter the name of the VM partition (keep it empty if not required): " sda3
echo ""
echo "What filesystem would you like to install??"
echo "1)ext4"
echo "2)btrfs"
while true; do
  read -p "Enter options 1 or 2: " filesys
    case $filesys in
        [1]* )
            echo "formatting using ext4"
        break;;
        [2]* )
            echo "formatting using btrfs"
        break;;
        * ) echo "Please select 1 or 2";;
    esac
done

# ------------------------------------------------------
# Sync time
# ------------------------------------------------------
timedatectl set-ntp true

# ------------------------------------------------------
# Format partitions
# ------------------------------------------------------
#mkfs.fat -F 32 /dev/$sda1;
if [ $filesys -eq 2 ];then
  mkfs.btrfs -f /dev/$sda2
  # mkfs.btrfs -f /dev/$sda3

  # ------------------------------------------------------
  # Mount points for btrfs
  # ------------------------------------------------------
  mount /dev/$sda2 /mnt
  btrfs su cr /mnt/@
  btrfs su cr /mnt/@cache
  btrfs su cr /mnt/@home
  btrfs su cr /mnt/@snapshots
  btrfs su cr /mnt/@log
  umount /mnt

  mount -o compress=zstd:1,noatime,subvol=@ /dev/$sda2 /mnt
  mkdir -p /mnt/{efi,home,.snapshots,var/{cache,log}}
  mount -o compress=zstd:1,noatime,subvol=@cache /dev/$sda2 /mnt/var/cache
  mount -o compress=zstd:1,noatime,subvol=@home /dev/$sda2 /mnt/home
  mount -o compress=zstd:1,noatime,subvol=@log /dev/$sda2 /mnt/var/log
  mount -o compress=zstd:1,noatime,subvol=@snapshots /dev/$sda2 /mnt/.snapshots
  mount /dev/$sda1 /mnt/efi
  # mkdir /mnt/vm
  # mount /dev/$sda3 /mnt/vm
fi

if [ $filesys -eq 1 ]; then
  mkfs.ext4 -f /dev/$sda2
  mount /dev/$sda2 /mnt
  mkdir /mnt/efi
  mount /dev/$sda1 /mnt/efi
fi

echo ""
echo ""
lsblk
echo ""
echo ""

# ------------------------------------------------------
# Install base packages
# ------------------------------------------------------
pacstrap -K /mnt base base-devel git linux linux-firmware vim openssh reflector rsync amd-ucode

# ------------------------------------------------------
# Generate fstab
# ------------------------------------------------------
genfstab -U /mnt >> /mnt/etc/fstab
cat /mnt/etc/fstab

# ------------------------------------------------------
# Install configuration scripts
# ------------------------------------------------------
mkdir /mnt/archinstall
cp 2-configuration.sh /mnt/archinstall/
cp 3-yay.sh /mnt/archinstall/
cp 4-zram.sh /mnt/archinstall/
cp 5-timeshift.sh /mnt/archinstall/
cp 6-preload.sh /mnt/archinstall/
cp snapshot.sh /mnt/archinstall/

# ------------------------------------------------------
# Chroot to installed sytem
# ------------------------------------------------------
arch-chroot /mnt #./archinstall/2-configuration.sh



#   ____             __ _                       _   _
#  / ___|___  _ __  / _(_) __ _ _   _ _ __ __ _| |_(_) ___  _ __
# | |   / _ \| '_ \| |_| |/ _` | | | | '__/ _` | __| |/ _ \| '_ \
# | |__| (_) | | | |  _| | (_| | |_| | | | (_| | |_| | (_) | | | |
#  \____\___/|_| |_|_| |_|\__, |\__,_|_|  \__,_|\__|_|\___/|_| |_|
#                         |___/
# by Stephan Raabe (2023)
# ------------------------------------------------------
clear
zoneinfo="Europe/Rome"
hostname="archlinux"
echo "Enter Username: must be in small letters, no capitals allowed."
read username

# ------------------------------------------------------
# Set System Time
# ------------------------------------------------------
ln -sf /usr/share/zoneinfo/$zoneinfo /etc/localtime
hwclock --systohc

# ------------------------------------------------------
# Update reflector
# ------------------------------------------------------
echo "Start reflector..."
reflector -l 30 -p https -a 2 --sort rate --save /etc/pacman.d/mirrorlist

# ------------------------------------------------------
# Synchronize mirrors
# ------------------------------------------------------
pacman -Syy

# ------------------------------------------------------
# Install Packages
# ------------------------------------------------------
#pacman --noconfirm -S xdg-desktop-portal-wlr efibootmgr networkmanager network-manager-applet dialog wpa_supplicant mtools dosfstools base-devel linux-headers avahi xdg-user-dirs xdg-utils gvfs gvfs-smb nfs-utils inetutils dnsutils bluez bluez-utils cups hplip alsa-utils pipewire pipewire-alsa pipewire-pulse pipewire-audio wireplumber pipewire-jack bash-completion openssh rsync reflector acpi acpi_call dnsmasq openbsd-netcat ipset firewalld flatpak sof-firmware nss-mdns acpid ntfs-3g terminus-font exa bat htop ranger zip unzip neofetch duf xorg xorg-xinit xclip xf86-video-amdgpu xf86-video-nouveau xf86-video-intel xf86-video-qxl brightnessctl pacman-contrib inxi
pacman --noconfirm -S noto-fonts-emoji ttf-dejavu noto-fonts noto-fonts-extra efibootmgr networkmanager refind-btrfs network-manager-applet bluez bluez-utils dosfstools mtools zip unzip brightnessctl inxi pacman-contrib reflector bat htop duf

# ------------------------------------------------------
# set lang utf8 US
# ------------------------------------------------------
echo "en_US.UTF-8 UTF-8" >> /etc/locale.gen
locale-gen
echo "LANG=en_US.UTF-8" >> /etc/locale.conf

# ------------------------------------------------------
# Set Keyboard
# ------------------------------------------------------

# ------------------------------------------------------
# Set hostname and localhost
# ------------------------------------------------------
echo "$hostname" >> /etc/hostname
echo "127.0.0.1 localhost" >> /etc/hosts
echo "::1       localhost" >> /etc/hosts
echo "127.0.1.1 $hostname.localdomain $hostname" >> /etc/hosts
clear

# ------------------------------------------------------
# Set Root Password
# ------------------------------------------------------
echo "Set root password"
passwd root

# ------------------------------------------------------
# Add User
# ------------------------------------------------------
echo "Add user $username"
useradd -m -G wheel $username
passwd $username

# ------------------------------------------------------
# Enable Services
# ------------------------------------------------------
systemctl enable NetworkManager
systemctl enable bluetooth
#systemctl enable cups.service
# systemctl enable sshd
# systemctl enable avahi-daemon
systemctl enable reflector.timer
systemctl enable fstrim.timer
# systemctl enable firewalld
# systemctl enable acpid

# ------------------------------------------------------
# Grub installation
# ------------------------------------------------------

# ------------------------------------------------------
# Add btrfs and setfont to mkinitcpio
# ------------------------------------------------------
# Before: BINARIES=()
# After:  BINARIES=(btrfs setfont)
if [ $filesys -eq 2]; then
  sed -i 's/MODULES=()/MODULES=(btrfs)/g' /etc/mkinitcpio.conf
  mkinitcpio -p linux
fi
if [ $filesys -eq 1]; then
  mkinitcpio -p linux
fi

# ------------------------------------------------------
# Add user to wheel
# ------------------------------------------------------
clear
echo "Uncomment %wheel group in sudoers (around line 85):"
echo "Before: #%wheel ALL=(ALL:ALL) ALL"
echo "After:  %wheel ALL=(ALL:ALL) ALL"
echo ""
read -p "Open sudoers now?" c
EDITOR=vim sudo -E visudo
usermod -aG wheel $username

# ------------------------------------------------------
# Copy installation scripts to home directory
# ------------------------------------------------------
cp /archinstall/3-yay.sh /home/$username
cp /archinstall/4-zram.sh /home/$username
cp /archinstall/5-timeshift.sh /home/$username
cp /archinstall/6-preload.sh /home/$username
cp /archinstall/snapshot.sh /home/$username

clear
echo "     _                   "
echo "  __| | ___  _ __   ___  "
echo " / _' |/ _ \| '_ \ / _ \ "
echo "| (_| | (_) | | | |  __/ "
echo " \__,_|\___/|_| |_|\___| "
echo "                         "
echo ""
echo "Please find the following additional installation scripts in your home directory:"
echo "- yay AUR helper: 3-yay.sh"
echo "- zram swap: 4-zram.sh"
echo "- timeshift snapshot tool: 5-timeshift.sh"
echo "- preload application cache: 6-preload.sh"
echo ""
echo "Please exit & shutdown (shutdown -h now), remove the installation media and start again."
echo "Important: Activate WIFI after restart with nmtui."
